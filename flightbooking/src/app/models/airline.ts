export interface Airline{
    airlineId:number;
    name:string;
    phone:string;
    adress:string;
    logo:string;
    active:number;
}