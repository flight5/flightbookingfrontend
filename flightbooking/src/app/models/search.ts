export interface SearchItem{
    airlineId:number;
    scheduleId:number;
    flightNumber :string;
    name:string;
    phone:string;
    logo:string;
    fromPlace:string;
    toPlace:string;
    startDate:string;
    ticketCost:number;
    journeyDays:number;
    endDate:number;

}



export interface Search{
    depature?:SearchItem[];
    returns?:SearchItem[];
}


export interface SelectedFlights{
    depature?:SearchItem;
    returns?:SearchItem;
    totolCost?:number;
}

