export class ScheduleDb {
    AirlineId!:number;
    FlightNumber!:string;
    FromPlace!:string;
    ToPlace!:string;
    StartDate!:string;
    EndDate!:string; 
    ScheduledDays!:string; 
    TotalRows!:number; 
    InstrumentsUsed!:string; 
    MealsType!:number; 
    TicketCost!:number; 
    IsActive!:number;
    BussinesSeats!:number;

  }