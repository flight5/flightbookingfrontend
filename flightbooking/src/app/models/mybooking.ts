export interface MyBooking{
    bookingId: number;
    billingId: number;
    fromPlace: string;
    toplace: string,
    journeyDate: string,
    totalSeats: number,
    type: number,
    bookingStatus: number,
    pnr: string,
    canceled: number,
    scheduleId: number,
    selectedSeats: string,
    amount: number,
    
}


export interface PassengerDetails
{

    Name:string;
    Gender:string;
    Age:number;
    MealType:number;
    SeatNumber:string;
}

export interface Booking
{

    BillingId:number;
    TotalSeats:number;
    Type:string;
    ScheduleId:number;
    Passenger:PassengerDetails[];
}


export interface priceDetail
{
    depeprice :number;
    retprice:number;
    deptotal:number;
    rettotal:number;
    total:number;
    disprice:number;
}

export interface FinalAmount
{
    total :number;
    discount:number;
    final:number;
    
}
