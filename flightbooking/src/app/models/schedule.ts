export interface Schedule{
    scheduleid:number;
    airlineId:number;
    flightNumber:string;
    fromPlace:string;
    toPlace :string;
    startDate : string;
    endDate :string;
    scheduledDays :string;
    bussinesSeats: number;
  totalSeats: number;
  totalRows: number;
  instrumentsUsed: "string",
  mealsType: number;
  ticketCost: number;
  isActive: number;

}