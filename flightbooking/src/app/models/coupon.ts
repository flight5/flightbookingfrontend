export interface Coupon{
    couponCode:string;
    percentage:number;
    maxCount:number;
    expiry:string;
}