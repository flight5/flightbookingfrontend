export class CouponForm{
    CouponCode!:string;
    Percentage!:number;
    MaxCount!:number;
    Expiry!:string;
}