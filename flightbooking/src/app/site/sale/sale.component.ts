import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Booking, FinalAmount, priceDetail } from 'src/app/models/mybooking';
import { SelectedFlights } from 'src/app/models/search';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})
export class SaleComponent implements OnInit {

  flights:any;
  selectedItems!:SelectedFlights;
  BillingId!:number;
  form: FormGroup;
  retform: FormGroup;
  priceAmount!:priceDetail;
  isPasengerUpdated:boolean=false;
  couponCode!:string;
  finalamount!:FinalAmount;


  constructor( public formbuilder: FormBuilder,
    public router: Router,
    private searchService:SearchService) {


      this.form = this.formbuilder.group({
        credentials: this.formbuilder.array([]),
      });

      this.retform = this.formbuilder.group({
        returnpass: this.formbuilder.array([])
      });
      
      let flightsDet = localStorage.getItem("selectedFlights");
      if(flightsDet !=null)
      {
        this.flights =JSON.parse(flightsDet);
        
      }else{
        this.router.navigate(["/"]);
      }
      
      let billingData  = localStorage.getItem("billingId");
      this.BillingId = (billingData ==null)?0:+billingData;

      if(this.BillingId ==0)
      {
        this.router.navigate(["/preview"]);

      }

     }

  ngOnInit(): void {
 
    this.getSlectedFlights();
  }

  public getSlectedFlights()
  {
    this.searchService.selectedFlights(this.flights)
    .subscribe((selected:SelectedFlights) =>
    {

      this.selectedItems = selected;
      this.priceAmount =  { depeprice :(selected.depature?.ticketCost)?selected.depature?.ticketCost:0,
                            retprice  :(selected.returns?.ticketCost)?selected.returns?.ticketCost:0,
                            deptotal :1 ,
                            rettotal :1,
                            total :  (selected?.totolCost)?selected?.totolCost:0,
                            disprice : 0
                           }
    });
  

  }

  addCreds() {
    const creds = this.form.controls['credentials'] as FormArray;
    creds.push(this.formbuilder.group({
      Name: ['',Validators.required],
      Age: ['',Validators.required],
      Gender: ['',Validators.required],
      SeatNumber: ['',Validators.required],
      MealType: ['',Validators.required]
    }));
    return true;
  }
  returnPassenger()
  {
    const retForm = this.retform.controls['returnpass'] as FormArray;
    retForm.push(this.formbuilder.group({
      Name: ['',Validators.required],
      Age: ['',Validators.required],
      Gender: ['',Validators.required],
      SeatNumber: ['',Validators.required],
      MealType: ['',Validators.required]
    }));
    return true;

  }
  completeBookins()
  {


    let creds = this.form.controls['credentials'] as FormArray;

    let priceUpdate = this.priceAmount;
    if(creds.length >0){

      let bookingData:Booking ={
        BillingId: 0,
        TotalSeats: 0,
        Type: '',
        ScheduleId: 0,
        Passenger: []
      };
   
       let totalDep = creds.length ;

        if(totalDep !=  this.priceAmount.deptotal){
          priceUpdate.depeprice = this.priceAmount.depeprice*totalDep;
          priceUpdate.deptotal =totalDep;
         }

          bookingData.BillingId =   this.BillingId;
          bookingData.ScheduleId =   this.flights.depatureId;
          bookingData.TotalSeats =  totalDep;
          bookingData.Type = "Departure";
          for(let i=0;i<totalDep;i++)
          {
            var item = creds.at(i).value;
            bookingData.Passenger.push(item);
          }

        this.updateBookings(bookingData);
    }else{
      priceUpdate.depeprice =0;
      priceUpdate.deptotal = 0;
    }

    let retForm = this.retform.controls['returnpass'] as FormArray;

    
     if(retForm.length >0)
      {
        let bookingRetData:Booking ={
          BillingId: 0,
          TotalSeats: 0,
          Type: '',
          ScheduleId: 0,
          Passenger: []
        };
        let totalret = retForm.length ;
        if(totalret !=  this.priceAmount.rettotal){

           priceUpdate.retprice =this.priceAmount.retprice*totalret;
           priceUpdate.rettotal =totalret;
        }

        bookingRetData.BillingId =   this.BillingId;
        bookingRetData.ScheduleId =   this.flights.depatureId;
        bookingRetData.TotalSeats =  totalret;
        bookingRetData.Type = "Return";
        for(let i=0;i<totalret;i++)
        {
          var item = creds.at(i).value;
          bookingRetData.Passenger.push(item);
        }

          this.updateBookings(bookingRetData);

      }else{
        priceUpdate.retprice =0;
        priceUpdate.rettotal =0;
      }
      priceUpdate.total = priceUpdate.retprice +priceUpdate.depeprice;
      this.priceAmount = priceUpdate;


  }

  public updateBookings(bookData:Booking)
  {
     this.searchService.updateBooking(bookData)
     .subscribe((res: any) => {
          this.isPasengerUpdated = true;
          this.refreshPayment();

    });

  }

    public completePayments():void
    {
      this.searchService.completePayment(this.BillingId)
      .subscribe((res: string) => {
        localStorage.removeItem("selectedFlights");
        localStorage.removeItem("billingId");
        this.router.navigate(["/myAccounts"]);
       });
    }

    public applyDisocunt():void
    {
      if(this.couponCode)
      {
         this.searchService.applyDiscountCoupon(this.couponCode,this.BillingId)
         .subscribe((res: any) => {
             this.refreshPayment();
          });

      }
      
    }

    public refreshPayment()
    {

      this.searchService.refreshPayments(this.BillingId)
      .subscribe((res: any) => {
        this.finalamount ={total:res?.totalAmount,discount:res?.discount,final:res?.fInalAmount}
       });

    }
    trackByFn()
    {

    }
}
