import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MyBooking } from 'src/app/models/mybooking';
import { MybookingService } from 'src/app/services/mybooking.service';

@Component({
  selector: 'app-mybooking',
  templateUrl: './mybooking.component.html',
  styleUrls: ['./mybooking.component.css']
})
export class MybookingComponent implements OnInit {
  booking:MyBooking[]=[];
  constructor(private mybookingservice:MybookingService,
    public router: Router,) { }
  cancelfn(pnr:string)
  {
    this.mybookingservice.cancelpnr(pnr)
    .subscribe((res: any) => {
      this.getmyBooking();
    });
    
  }
  ngOnInit(): void {
    this.getmyBooking();
  }


  getmyBooking():void{
    this.mybookingservice.getmyBooking()
      .subscribe(booking=>{
        this.booking=booking;
        console.log(this.booking);

      });
  }
  search(id:string)
  {
    
    this.mybookingservice.search(id)
    .subscribe((res=>this.booking=res) )
   
  }
  logout()
  {

    localStorage.clear();
    sessionStorage.clear();
    this.router.navigate(["/login/3"]);
    return true;
  }
}
