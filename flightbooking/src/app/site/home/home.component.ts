import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Search } from 'src/app/models/search';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  SearchForm!:FormGroup;
  search!:Search;
  depatureId!:number;
  retrunId!:number;
  flightSelected:boolean=false;
  constructor(
    public formbuilder: FormBuilder,
    public router: Router,
    private searchService:SearchService) { }

  ngOnInit(): void {

    this.SearchForm=  this.formbuilder.group({
      FromPlace: ['', Validators.required],
      ToPlace: ['', Validators.required],
      roundTrip: ['', Validators.required],
      DepatureDate: [''],
      returnDate: ['']

      });

      
  }

  public setcurrentlyClickedCardIndex(cardIndex: number,type:string): void {
    if(type =='departure')
    {
      this.depatureId = cardIndex;
    }else{
      this.retrunId = cardIndex;

    }
    this.checkFlightSelected();

  }

  public checkIfCardIsClicked(cardIndex: number,type:string): boolean {
    if(type =='departure')
    {
      return (this.depatureId == cardIndex)?true:false;
    }else{
      return (this.retrunId == cardIndex)?true:false;

    }

    return false;

  }


  public checkFlightSelected() {
 
    this.flightSelected =false;
    if(this.depatureId !=null ){
      this.flightSelected =true;

    }
    if(this.retrunId !=null ){
      this.flightSelected =true;

    }

  }

  public ShowBookingPage():void
  {
  
  
    let tcks =  {depatureId :this.depatureId,returnId:this.retrunId}
    localStorage.setItem("selectedFlights", JSON.stringify(tcks));
    this.router.navigate(['/preview']);

  }


  getSearch():void{

    this.searchService.getSearch(this.SearchForm.value)
     .subscribe(search=>(this.search=search))

  }

}
 