import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { SelectedFlights } from 'src/app/models/search';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {
  flights:any;
  selectedItems!:SelectedFlights;
  
  constructor( public formbuilder: FormBuilder,
    public router: Router,
    private searchService:SearchService) {
      let flightsDet = localStorage.getItem("selectedFlights");
      if(flightsDet !=null)
      {
        this.flights =JSON.parse(flightsDet);
        
      }else{
        this.router.navigate(["/"]);
      }
   


     }

  ngOnInit(): void {
    this.getSlectedFlights();

  }

  public getSlectedFlights()
  {
    this.searchService.selectedFlights(this.flights)
    .subscribe(selected=>(this.selectedItems=selected))
  }

  public initateBooking()
  {
  
     if(sessionStorage.getItem("token"))
     {
       this.searchService.getBillingDetails(this.flights);
     }else{
      this.router.navigate(["/login/3"]);
     }
  }

  public continuePayment()
  {

  }

}
