import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-redirect',
  template: ''
})
export class RedirectComponent implements OnInit {
  flights:any;

  constructor( public formbuilder: FormBuilder,
    public router: Router,
    private searchService:SearchService) {
      let flightsDet = localStorage.getItem("selectedFlights");
      if(flightsDet !=null)
      {
        this.flights =JSON.parse(flightsDet);
      }else{
        this.router.navigate(["/"]);
      }

     }
  ngOnInit(): void {
   this.initateBooking();
  }

  public initateBooking()
  {
     if(sessionStorage.getItem("token"))
     {
       this.searchService.getBillingDetails(this.flights);
     }else{
      this.router.navigate(["/login/3"]);
     }
  }


}
