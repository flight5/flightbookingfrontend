import { Component, OnInit } from '@angular/core';
import { Coupon } from 'src/app/models/coupon';
import { CouponService } from 'src/app/services/coupon.service';

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.css']
})
export class CouponComponent implements OnInit {
  coupons:Coupon[]=[];
  constructor(private couponService:CouponService) { }

  ngOnInit(): void {
    this.getCoupon();
  }
  getCoupon():void{
    this.couponService.getCoupon()
    .subscribe(coupons=>(this.coupons=coupons));
  }


}
