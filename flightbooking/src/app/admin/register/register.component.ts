import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginserviceService } from 'src/app/services/loginservice.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  signupForm!: FormGroup;
  redirectPath!:string;

  constructor(
    public formbuilder: FormBuilder,
    public router: Router,
    public registerService:LoginserviceService,
    public route:ActivatedRoute

  ) { }

  ngOnInit(): void {
    let redirect = this.route.snapshot.paramMap.get("id");
    this.redirectPath = (redirect ==null)?"1":redirect;

    
    this.signupForm=this.formbuilder.group({
            Name: ['', Validators.required],
            Email: ['', Validators.required],
            PhoneNumber: ['', Validators.required],
            Gender: ['', Validators.required],
            UserName: ['', Validators.required],
            Password: ['', [Validators.required, Validators.minLength(6)],
          ]
    })
  }
  registerUser()
  {
     this.registerService.register(this.signupForm.value,this.redirectPath);
  }

}
