import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { ScheduleService } from 'src/app/services/schedule.service';

@Component({
  selector: 'app-add-schedule',
  templateUrl: './add-schedule.component.html',
  styleUrls: ['./add-schedule.component.css']
})
export class AddScheduleComponent implements OnInit {
   ScheduleForm!: FormGroup;
   aid!:string;

  constructor(
    public formbuilder: FormBuilder,
    public router: Router,
    public scheduleService:ScheduleService,
    public route:ActivatedRoute
    ) { }

  ngOnInit(): void {
    var idval = this.route.snapshot.paramMap.get("id");
    if(idval ==null){
      this.router.navigate(['/dashboard']);
    }
    this.aid = (idval ==null)?"":idval;

    this.ScheduleForm=this.formbuilder.group({
      FlightNumber: ['', Validators.required],
      TotalSeats:['', Validators.required],
      FromPlace: ['', Validators.required],
      ToPlace: ['', Validators.required],
      StartDate: ['', Validators.required],
      EndDate: ['', Validators.required], 
      ScheduledDays: ['', Validators.required], 
      TotalRows: ['', Validators.required], 
      InstrumentsUsed: ['', Validators.required], 
      MealsType: [ Validators.required], 
      TicketCost: ['', Validators.required], 
      IsActive: ['', Validators.required],
      BussinesSeats: ['', Validators.required]
    });
  }

  createSchedule()
  {
    this.scheduleService.AddNewSchedule(this.ScheduleForm.value,this.aid);
  }

}
