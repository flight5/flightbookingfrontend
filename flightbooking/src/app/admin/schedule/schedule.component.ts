import { Component, OnInit } from '@angular/core';
import { Schedule } from 'src/app/models/schedule';
import { ScheduleService } from 'src/app/services/schedule.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {
  schedules:Schedule[]=[];
  constructor(private scheduleservice:ScheduleService) { }

  ngOnInit(): void {
    this.getSchedule();
  }
  getSchedule():void{
    this.scheduleservice.getSchedule()
    .subscribe(schedules=>(this.schedules=schedules));
  }

}
