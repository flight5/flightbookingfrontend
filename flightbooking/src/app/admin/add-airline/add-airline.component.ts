import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AirlineService } from 'src/app/services/airline.service';

@Component({
  selector: 'app-add-airline',
  templateUrl: './add-airline.component.html',
  styleUrls: ['./add-airline.component.css']
})
export class AddAirlineComponent implements OnInit {
  AddAirlineForm!: FormGroup;

  constructor(public formbuilder: FormBuilder,
    public router: Router,
    private airlineservice:AirlineService

    ) { }

  ngOnInit(): void {
    this.AddAirlineForm=this.formbuilder.group({
      Name: ['', Validators.required],
      Phone: ['', Validators.required],
      Adress: ['', Validators.required],
      logo: ['', Validators.required],
      Active: ['', [Validators.required, Validators.minLength(6)],
    ]
})
  }
  addAirline()
  {
    this.airlineservice.addAirline(this.AddAirlineForm.value)
    .subscribe((res: any) => {
      this.router.navigate(["/dashboard"]);
    });


  }

}
