import { Component, OnInit } from '@angular/core';
import { Airline } from 'src/app/models/airline';
import { AirlineService } from 'src/app/services/airline.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  Airlines:Airline[]=[];

  constructor(private airlineservice:AirlineService) { }

  ngOnInit(): void {
    this.getAirline();
  }
  getAirline():void{
    this.airlineservice.getAirline()
    .subscribe(airline=>(this.Airlines=airline));
  }
  changeAirline(airlineid:number,active:number)
  {
    let isActive = 1;
    if(active ==1){
      isActive =0;
    }
    this.airlineservice.changeAirlineStatus(airlineid,isActive)

  }

  deleteAirline(airlineid:number)
  {
    this.airlineservice.deleteAirlineData(airlineid)

  }
}
