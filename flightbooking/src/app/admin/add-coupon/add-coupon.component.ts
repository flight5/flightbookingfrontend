import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CouponService } from 'src/app/services/coupon.service';

@Component({
  selector: 'app-add-coupon',
  templateUrl: './add-coupon.component.html',
  styleUrls: ['./add-coupon.component.css']
})
export class AddCouponComponent implements OnInit {
  AddCouponForm!: FormGroup;
  constructor(public formbuilder: FormBuilder,
    public router: Router,
    private couponservice:CouponService
    ) { }

  ngOnInit(): void {
    this.AddCouponForm=this.formbuilder.group({
      CouponCode: ['', Validators.required],
      Percentage: ['', Validators.required],
      MaxCount: ['', Validators.required],
      Expiry: ['', Validators.required]
      
    
})
  }
  addCoupon()
  {
    this.couponservice.addCoupon(this.AddCouponForm.value)
    .subscribe((res: any) => {
      this.router.navigate(["/coupons"]);
    });


  }

}
