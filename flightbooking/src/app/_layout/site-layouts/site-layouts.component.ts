import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-site',
  templateUrl: './site-layouts.component.html',
  styleUrls: ['./site-layouts.component.css']
})
export class SiteLayoutsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
