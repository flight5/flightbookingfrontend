import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteLayoutsFooterComponent } from './site-layouts-footer.component';

describe('SiteLayoutsFooterComponent', () => {
  let component: SiteLayoutsFooterComponent;
  let fixture: ComponentFixture<SiteLayoutsFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiteLayoutsFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteLayoutsFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
