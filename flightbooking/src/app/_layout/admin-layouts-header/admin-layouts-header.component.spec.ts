import { ComponentFixture, TestBed } from '@angular/core/testing';


import { AdminLayoutsHeaderComponent } from './admin-layouts-header.component';

describe('AdminLayoutsHeaderComponent', () => {
  let component: AdminLayoutsHeaderComponent;
  let fixture: ComponentFixture<AdminLayoutsHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminLayoutsHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLayoutsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
