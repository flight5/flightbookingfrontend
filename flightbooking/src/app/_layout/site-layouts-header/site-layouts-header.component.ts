import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';


@Component({
  selector: 'app-site-header',
  templateUrl: './site-layouts-header.component.html',
  styleUrls: ['./site-layouts-header.component.css']
})
export class SiteLayoutsHeaderComponent implements OnInit {
  userLoggedIn =false;
   session:string;
  constructor( public fb: FormBuilder,
    public router: Router) {
       let ses = sessionStorage.getItem('token');
       this.session= (ses==null)?"":ses;
     }

  ngOnInit(): void {
   if(this.session.length >0){
     this.userLoggedIn =true;
   }

  }
 public logoutAction():void
 {
   sessionStorage.clear();
   this.router.navigate(["/"]);
 }
}
