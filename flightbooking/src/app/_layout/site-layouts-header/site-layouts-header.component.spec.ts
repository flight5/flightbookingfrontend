import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteLayoutsHeaderComponent } from './site-layouts-header.component';

describe('SiteLayoutsHeaderComponent', () => {
  let component: SiteLayoutsHeaderComponent;
  let fixture: ComponentFixture<SiteLayoutsHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiteLayoutsHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteLayoutsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
