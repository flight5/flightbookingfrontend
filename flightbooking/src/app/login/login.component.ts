import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginserviceService } from '../services/loginservice.service';
import { PreviewComponent } from '../site/preview/preview.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  signinForm: FormGroup
  redirectPath!:string

  constructor(   
    public fb: FormBuilder,
    public router: Router,
    public authService: LoginserviceService,
    public route:ActivatedRoute

    ) {

    this.signinForm = this.fb.group({
      userName: [''],
      password: [''],
    });
 
   }

  ngOnInit(): void {
     let redirect = this.route.snapshot.paramMap.get("id");
     this.redirectPath = (redirect ==null)?"1":redirect;


  }

  loginUser() {
    this.authService.signIn(this.signinForm.value)
    .subscribe((res: any) => {
      if(this.redirectPath =="2")
      { 
       
          localStorage.setItem('token', res.access_token);
          localStorage.setItem('referesh', res.refresh_token);
          this.router.navigate(['/dashboard']);
           
      }
      
      else if(this.redirectPath =="1"  || this.redirectPath =="3" ) {

          sessionStorage.setItem('token', res.access_token);
          sessionStorage.setItem('referesh', res.refresh_token);
          if(this.redirectPath =="3")
          {
            this.router.navigate(['/redirect/booking']);

 
          }else{
            this.router.navigate(['/']);
          }

      }
     



     
   });

  }
  


}
