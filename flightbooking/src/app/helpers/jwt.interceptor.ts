import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { LoginserviceService } from '../services/loginservice.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: LoginserviceService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if user is logged in and request is to api url
        let currentUser = localStorage.getItem("token");
        let siteUser = sessionStorage.getItem("token");
        let isLoggedIn = null;
        if(currentUser != null){
             isLoggedIn = currentUser;
        }
        if(siteUser != null){
             isLoggedIn = siteUser;
        }



        const isApiUrl = true;
        //const isApiUrl = request.url.startsWith();
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${isLoggedIn}`
                }
            });
        }

        return next.handle(request);
    }
}