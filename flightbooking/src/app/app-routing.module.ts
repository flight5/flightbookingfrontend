import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SiteLayoutsComponent } from './_layout/site-layouts/site-layouts.component';
import { AdminLayoutsComponent } from './_layout/admin-layouts/admin-layouts.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { HomeComponent } from './site/home/home.component';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './admin/register/register.component';
import { AuthGuard } from './helpers';
import { AddAirlineComponent } from './admin/add-airline/add-airline.component';
import { ScheduleComponent } from './admin/schedule/schedule.component';
import { ScheduleDetailsComponent } from './admin/schedule-details/schedule-details.component';
import { AddScheduleComponent } from './admin/add-schedule/add-schedule.component';
import { CouponComponent } from './admin/coupon/coupon.component';
import { AddCouponComponent } from './admin/add-coupon/add-coupon.component';
import { MybookingComponent } from './site/mybooking/mybooking.component';
import { SiteGuard } from './helpers/site.guard';
import { PreviewComponent } from './site/preview/preview.component';
import { SaleComponent } from './site/sale/sale.component';
import { RedirectComponent } from './site/redirect/redirect.component';

const routes: Routes = [
      //Site routes goes here  //canActivate:[SiteGuard]
      { 
        path: '', 
        component: SiteLayoutsComponent,
        children: [
          { path: '', component: HomeComponent,pathMatch: 'full'},
          { path: 'preview', component: PreviewComponent },
          { path: 'myAccounts', component: MybookingComponent ,canActivate:[SiteGuard]},  
          { path: 'sale', component: SaleComponent,canActivate:[SiteGuard]},
          { path: 'redirect/booking', component: RedirectComponent ,canActivate:[SiteGuard]}



        ]
    },

    { 
      path: '',
      component: AdminLayoutsComponent, 
      children: [

        { path: 'dashboard', component: DashboardComponent,canActivate: [AuthGuard] },
        { path: 'airline', component: AddAirlineComponent , canActivate: [AuthGuard]},
        { path: 'schedules', component: ScheduleComponent,canActivate: [AuthGuard] },
        {path: 'coupons', component: CouponComponent,canActivate: [AuthGuard]},
        {path: 'addcoupons', component: AddCouponComponent,canActivate: [AuthGuard]},
        { path: 'schedule/:id', component: ScheduleDetailsComponent,canActivate: [AuthGuard] },
        { path: 'schedule/add/:id', component: AddScheduleComponent,canActivate: [AuthGuard] }
      ]
    },
    {path:'login/:id',component:LoginComponent},
    {path:'register/:id',component:RegisterComponent},
   

    { path: '**', redirectTo: '' }

    //{ path: '', redirectTo: '/login', pathMatch: 'full'}  canActivate: [AuthGuard]

];
 


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
