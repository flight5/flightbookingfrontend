import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MyBooking } from '../models/mybooking';

@Injectable({
  providedIn: 'root'
})
export class MybookingService {
  endpoint: string = 'https://localhost:5001/api';
  constructor(private http:HttpClient,
    public router: Router) { }

  getmyBooking():Observable<MyBooking[]>{

    return this.http.get<MyBooking[]>(`${this.endpoint}/myall/bookings`);
  }

  cancelpnr(pnr:string):Observable<any>{
     return this.http.get<any>(`${this.endpoint}/pnr/cancel?pnr=`+pnr)
    
  }
  search(SearchItems:string):Observable<any>
  {
    
    if(SearchItems.includes('@'))
    {
      return this.http.get<any>(`${this.endpoint}/search/bookings?SearchItems=`+SearchItems+`&SearchType=email`)
      
      
    }
    else{
      
      return this.http.get<any>(`${this.endpoint}/search/bookings?SearchItems=`+SearchItems+`&SearchType=pnr`)
      

     
    }
  }
}
