import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Schedule } from '../models/schedule';
import { ScheduleDb } from '../models/ScheduleDb';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {
  endpoint: string = 'https://localhost:5001/admin';

  constructor(
    private http:HttpClient,
    public router: Router
  ) { }
  getSchedule():Observable<Schedule[]>{

    return this.http.get<Schedule[]>(`${this.endpoint}/schedule/getallschedule`);

  }

  AddNewSchedule(scheData:ScheduleDb,airlineId:string){
    scheData.IsActive=1;
    scheData.AirlineId = +airlineId;
    scheData.MealsType=+scheData.MealsType;
    return this.http
    .post<any>(`${this.endpoint}/schedule/add`, scheData)
    .subscribe((res: any) => {

      this.router.navigate(["/schedules"]);
    });

  }
}
