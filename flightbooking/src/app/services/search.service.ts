import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BillingObj } from '../models/BillingObj';
import { Booking } from '../models/mybooking';
import { Search, SelectedFlights } from '../models/search';
import { SearchForm } from '../models/SearchForm';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  endpoint: string = 'https://localhost:5001/api';
  constructor(
    private http:HttpClient,
    public router: Router
  ) { }

  getSearch(sdata:SearchForm):Observable<Search>{

    let params = new HttpParams()
    .set("roundTrip",+sdata.roundTrip)
    .set("FromPlace",sdata.FromPlace)
    .set("ToPlace",sdata.ToPlace);
    if(sdata.DepatureDate.length >0)
    {
      params = params.append("DepatureDate",sdata.DepatureDate);
    }
    if(sdata.returnDate.length > 0)
    {
      params = params.append("returnDate",sdata.returnDate);
    }
    
    
    return this.http.get<Search>(`${this.endpoint}/search`,{params});
  }

  public selectedFlights(ids:any):Observable<SelectedFlights>
  {
    let params = new HttpParams();
     if(ids.depatureId !=null)
     {   
       params =params.set("departureId",+ids.depatureId);
     }
     if(ids.returnId !=null)
     {   
       params =params.set("returnId",+ids.returnId);

     }
     console.log(params.toString());

     
     return this.http.get<SelectedFlights>(`${this.endpoint}/schedule/selected`,{params});


  }

   public getBillingDetails(ids:any):void
   {
     
      let pramas =  new BillingObj();
        if(ids.depatureId !=null)
      {   
        pramas.Departure = ids.depatureId;
      }
      if(ids.returnId !=null)
      {   
        pramas.Return = ids.returnId;
      }

      this.http.post<any>(`${this.endpoint}/booking/started`, pramas)
     
      .subscribe((res: any) => {
        let billingId = res?.billingId;
        if(billingId != null){
          localStorage.setItem("billingId",billingId);
          this.router.navigate(['/sale']);
        }else{
          this.router.navigate(['/preview']);

        }
  
      });
  

   }

   public updateBooking(bookData:Booking):Observable<any>
   {


     
      return this.http.post<any>(`${this.endpoint}/booking`, bookData);

   }

   public applyDiscountCoupon(coupon:string,billingId:number):Observable<any>
   {
     
      const params =new HttpParams()
      .set ('Coupon','coupon')
      .set('BillId','billingId')
      
    
     
    return this.http.post<any>(`${this.endpoint}/coupon/apply?Coupon=`+coupon+`&BillId=`+billingId, params);
   

   }

  public refreshPayments(billingId:number)
  {
     
     return this.http.get<any>(`${this.endpoint}/booking/pricedetails?BillingId=`+billingId);
   
  }
  public completePayment(billingId:number)
  {
    let body = new URLSearchParams();
    body.set('BillingId', ""+billingId);
    const params =new HttpParams()
      .set ('BillingId','billingId')
      
     
    return this.http.post<any>(`${this.endpoint}/booking/pay?BillingId=`+billingId, params);
  }
  
   
}
