import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Airline } from '../models/airline';
import { AirlineForm } from '../models/airlineForm';

@Injectable({
  providedIn: 'root'
})
export class AirlineService {
  endpoint: string = 'https://localhost:5001/admin';

  constructor(
    private http:HttpClient,
    public router: Router
  ) { }

  getAirline():Observable<Airline[]>{

    return this.http.get<Airline[]>(`${this.endpoint}/airline/getallairline`);

  }
  addAirline(airdata:AirlineForm):Observable<any>
  {

    return this.http
    .post<any>(`${this.endpoint}/airline/add`, airdata);
  
  }

  changeAirlineStatus(airlineId:number, isActive:number)
  {

    /*let body = new URLSearchParams();
      body.set('airlineId', ""+airlineId);
      body.set('active', ""+isActive);

      let options = {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      };*/
      const params =new HttpParams()
      .set ('airlineId','airlineId')
      .set('active','isActive')

    return this.http
    .put<any>(`${this.endpoint}/airline/status?airlineId=`+airlineId+`&active=`+isActive, params)
    .subscribe((res: any) => {

      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(["/dashboard"]);
      });
    });

  }



  deleteAirlineData(airlineId:number)
  {

    /*let body = new URLSearchParams();
      body.set('airlineId', ""+airlineId);

      let options = {
          headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
          body:body
      };*/

    return this.http
    .delete<any>(`${this.endpoint}/airline/delete?airlineId=`+airlineId)
    .subscribe((res: any) => {
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate(["/dashboard"]);
      });

    });

  }



  
}
