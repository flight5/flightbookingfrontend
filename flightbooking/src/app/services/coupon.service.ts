import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Coupon } from '../models/coupon';
import { CouponForm } from '../models/couponForm';

@Injectable({
  providedIn: 'root'
})
export class CouponService {

  endpoint: string = 'https://localhost:5001/admin';

  constructor(
    private http:HttpClient,
    public router: Router
  ) { }
  getCoupon():Observable<Coupon[]>{

    return this.http.get<Coupon[]>(`${this.endpoint}/coupon/getallcoupon`);

  }
  addCoupon(coupondata:CouponForm):Observable<any>
  {

    return this.http.post<any>(`${this.endpoint}/coupon/add`, coupondata);
 
  }
}
