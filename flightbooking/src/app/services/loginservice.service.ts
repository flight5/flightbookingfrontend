import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { login } from '../models/login';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {

  endpoint: string = 'https://localhost:5001/auth';
  
  //headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
  currentUser = {};

  constructor(private http: HttpClient, public router: Router) {}

  
    // Sign-in
    signIn(user: login):Observable<any> {
      return this.http.post<any>(`${this.endpoint}/login`, user);
  
    }
        // Sign-up
   register(regObj:User,redirect:string) {
          //let options={headers:this.headers}
          if(redirect  =="2"){
            regObj.Role="Admin";

          }else{
            regObj.Role="User";

          }
          return this.http
            .post<any>(`${this.endpoint}/register`, regObj)
            .subscribe((res: any) => {
              this.router.navigate(['/login/'+redirect]);
            });
    
            
        }

   logout()
        {

          localStorage.clear();
          sessionStorage.clear();
          return true;
    }
    
    


    
  
      // Error
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
  
}
