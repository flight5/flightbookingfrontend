import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdminLayoutsComponent } from './_layout/admin-layouts/admin-layouts.component';
import { SiteLayoutsComponent } from './_layout/site-layouts/site-layouts.component';
import { SiteLayoutsHeaderComponent } from './_layout/site-layouts-header/site-layouts-header.component';
import { SiteLayoutsFooterComponent } from './_layout/site-layouts-footer/site-layouts-footer.component';
import { AdminLayoutsHeaderComponent } from './_layout/admin-layouts-header/admin-layouts-header.component';
import { AdminLayoutsFooterComponent } from './_layout/admin-layouts-footer/admin-layouts-footer.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { HomeComponent } from './site/home/home.component';
import { ReactiveFormsModule ,FormsModule} from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';
import { RegisterComponent } from './admin/register/register.component';
import { AddAirlineComponent } from './admin/add-airline/add-airline.component';
import { ErrorInterceptor, JwtInterceptor } from './helpers';
import { ScheduleComponent } from './admin/schedule/schedule.component';
import { ScheduleDetailsComponent } from './admin/schedule-details/schedule-details.component';
import { AddScheduleComponent } from './admin/add-schedule/add-schedule.component';
import { CouponComponent } from './admin/coupon/coupon.component';
import { AddCouponComponent } from './admin/add-coupon/add-coupon.component';
import { MybookingComponent } from './site/mybooking/mybooking.component';
import { PreviewComponent } from './site/preview/preview.component';
import { SaleComponent } from './site/sale/sale.component';
import { RedirectComponent } from './site/redirect/redirect.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminLayoutsComponent,
    SiteLayoutsComponent,
    SiteLayoutsHeaderComponent,
    SiteLayoutsFooterComponent,
    AdminLayoutsHeaderComponent,
    AdminLayoutsFooterComponent,
    DashboardComponent,
    HomeComponent,
    RegisterComponent,
    AddAirlineComponent,
    ScheduleComponent,
    ScheduleDetailsComponent,
    AddScheduleComponent,
    CouponComponent,
    AddCouponComponent,
    MybookingComponent,
    PreviewComponent,
    SaleComponent,
    RedirectComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
